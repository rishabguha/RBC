include("ModelVariables.jl")
include("DataGeneration.jl")
include("DataTransformation.jl")
include("DataAnalysis.jl")
include("ToLaTeXTable.jl")

              

function main()
    # QIa
    constants = setConstants(Dict("α" => 0.33, "β" => 0.984, "δ" => 0.025,
                                  "γ" => 0.004, "ρ" => 0.979, "σ" => 0.0072,
                                  "labor_share" => 0.20))

    runAnalysis(constants, ClassicRBC(), "QIa")

    # QI1a (less persistence)
    
    constants = setConstants(Dict("α" => 0.33, "β" => 0.984, "δ" => 0.025,
                                  "γ" => 0.004, "ρ" => 0.50, "σ" => 0.0072,
                                  "labor_share" => 0.20))

    runAnalysis(constants, ClassicRBC(), "QI1a")

    # QI1b (more persistence)

    constants = setConstants(Dict("α" => 0.33, "β" => 0.984, "δ" => 0.025,
                                  "γ" => 0.004, "ρ" => 0.999, "σ" => 0.0072,
                                  "labor_share" => 0.20))

    runAnalysis(constants, ClassicRBC(), "QI1b")

    #QI2a (variable Frisch with higher elasticity)
    constants = setConstants(Dict("α" => 0.33, "β" => 0.984, "δ" => 0.025,
                                  "γ" => 0.004, "ρ" => 0.979, "σ" => 0.0072,
                                  "labor_share" => 0.20))

    runAnalysis(constants, VariableFrisch(0.1), "QI2a")

    #QI2b (variable Frisch with infinite elasticity)
    runAnalysis(constants, VariableFrisch(0.0), "QI2b")

    #QI2c (variable Frisch with close to 0 elasticity)
    runAnalysis(constants, VariableFrisch(100.0), "QI2c")


end


