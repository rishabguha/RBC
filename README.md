# RBC.jl

A set of Julia functions to calibrate a classic RBC model. RBC.jl runs a series of analyses to test the effects of varying levels of TFP shock persistence and Frisch labor elasticity on macro-variable impulse-responses and cross-correlations. 


`ModelVariables.jl` contains the datatypes used to summarize the history and evolution of each macro variable in the model. `DataGeneration.jl` contains most of the actual model logic, and describes how the log-linearized RBC model is solved. `DataTransformation.jl` contains functions that take outputs from the log-linearized economy, and transforms them into extensive variables that can be compared to actual macro data. `DataAnalysis.jl` contains functions to plot the data, and analyze cross-correlations.

`ToLatexTable.jl` is a simple helper function to transform a Julia array into a .tex file that can be wrapped in a LaTeX `{tabular}` environment. 
