    import Gadfly.plot

#= Types to describe the different types of model variables (e.g.,
investment, consumption, etc.) and helper functions to operate on them =#

# type to hold the model constants
immutable ConstantValues
    α::Float64
    β::Float64
    δ::Float64
    γ::Float64
    ρ::Float64
    σ::Float64
    labor_share::Float64
end

# convenience function to create a constant type from a given dict
function setConstants(d::Dict{String,Float64})
    return ConstantValues(d["α"], d["β"], d["δ"], d["γ"], d["ρ"], d["σ"], d["labor_share"])
end


abstract ModelVariable

#= DeterministicVariable is a type that contains the name of the variable, the
time-series history of the variable, and λ which represents the variable as a
linear function of the state variables. λ is a dictionary associating each state
variable name with a coefficient that represents the effect of that state variable
on the response =#

immutable DeterministicVariable <: ModelVariable 
    name::String
    λ::Dict{String,Float64}
    history::Array{Float64,1}
end

# Response variables will be initialized without a history. State
# variables will be initialized with the initial value
DeterministicVariable(name, λ) = DeterministicVariable(name, λ, zeros(0))

# Stochastic variables also have the variance of a stochastic shock

immutable StochasticVariable <: ModelVariable
    name::String
    λ::Dict{String,Float64}
    history::Array{Float64,1}
    σ::Float64
end

# Simulated variables only need variable name and history
immutable SimulatedVariable <: ModelVariable
    name::String
    history::Array{Float64, 1}
end

SimulatedVariable(mv::ModelVariable) = SimulatedVariable(mv.name, mv.history)



# convenience function to make the λ dictionary 
function makeλ(λk, λa)
    return Dict("k" => λk, "a" => λa)
end



# increments v by one time step
function update{T<:ModelVariable}(v::T, state_variables)
    new_value  = 0
    for sv in state_variables
        new_value += v.λ[sv.name] * sv.history[end]
    end

    if (T==StochasticVariable)
        new_value += randn() * v.σ
    end
    
    # return a new copy of v, adding new_value to its history
    push!(v.history, new_value)
    return v
end           


# given a set of variables, simulate their history to a desired T

function simulateSystem(state_variables::Array,
                        response_variables::Array, T::Int64)
    for t=0:T
        # the state variables start out ahead of all the response variables,
        # so we need to give them 1 period to catch up
        if (t > 0)
            # it's important that the state variables get incremented first,
            # since they are the base for the other variables
            state_variables = map(sv -> update(sv, state_variables),
                                  state_variables)
        end    
        response_variables = map(rv -> update(rv, state_variables),
                                 response_variables)    
    end
    variables = [state_variables...,response_variables...]
    # convert to SimulatedVariables and turn into a dictionary keyed on
    # variable name
    return Dict(v.name => SimulatedVariable(v) for v in variables)
end



