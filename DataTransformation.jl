include("ModelVariables.jl")
include("DataGeneration.jl")

#= These functions transform the intensive 'tilde variables'
(where tilde x = log(x) - log(x*) into extensive real variables, and
apply an hp filter to detrend them =#

# calculate the intensive steady state for the economy specified
# by the constants
function calculateIntensiveSteadyState(constants::ConstantValues)
    α = constants.α
    β = constants.β
    δ = constants.δ
    γ = constants.γ
    ρ = constants.ρ
    l = constants.labor_share

    k = (l) * (((1+γ) - β * (1-δ))/(α * β))^(1/(α-1))
    y = (k^α) * (l^(1-α))
    i = (δ + γ) * k
    c = y - i
    R = -δ + α * (y/k)
    w = y / l
    y_l = y / l

    return Dict("k" => k, "y" => y, "i" => i, "c" => c,
                "a" => 1.0, "R" => R, "l" => l, "w"=> w, "y_l" => y_l)
end

# apply the hp filter with a given λ to the provided
# data series

function hpFilter(y, λ)
    T = length(y)

    first_row = vcat([1+λ, -2 * λ, λ], zeros(T-3))'
    second_row = vcat([-2 * λ, 1+ 5 * λ, -4 * λ, λ], zeros(T-4))'

    rows = [first_row, second_row]

    sliding_row = [λ, -4 * λ, 1+ 6 * λ, -4 * λ, λ]

    zeros_to_left = 0
    zeros_to_right = T-5
    
    while (zeros_to_right >= 0)
        new_row = vcat(zeros(zeros_to_left), sliding_row, zeros(zeros_to_right))'
        push!(rows, new_row)
        zeros_to_left += 1
        zeros_to_right -= 1
    end

    push!(rows, reverse(vec(second_row))')
    push!(rows, reverse(vec(first_row))')

    A = cat(1, rows...)

    τ = A \ y
    c = y - τ
    return c
end

function transformTildeVariable(tilde_variable::ModelVariable, 
                                constants::ConstantValues,
                                intensive_steady_states::Dict{String,Float64})
    # hp filter paramaeter
    λ = 1600
    
    tilde_history = tilde_variable.history
    name = tilde_variable.name
    
    T = length(tilde_history) - 1

    # exponentiate the tilde variable and multiply by steady state to get
    # the intensive value
    transformed_history = (exp(tilde_history) *
                           intensive_steady_states[name])

    # we need to change the non-labor variables from extensive to
    # intensive by dividing by the growth trend
    if (name != "l")
        transformed_history = transformed_history .* [(1+constants.γ)^t for t=0:T]
    end

    filtered_history = hpFilter(transformed_history, λ)
    
    return SimulatedVariable(name, filtered_history)
end

# wraps the transformation functions to generate simulated data from
# an economy

function generateSimulatedData(constants::ConstantValues, T::Int64, model_type::ModelType)
    tilde_variables = generateData(constants, T, SimulatedData(), model_type)
    intensive_steady_states = calculateIntensiveSteadyState(constants)

    # transform and filter each tilde variable and return the results
    data = Dict(k => transformTildeVariable(v, constants, intensive_steady_states)
                for (k,v) in tilde_variables)
    return data
end
