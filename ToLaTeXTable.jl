#= A simple function to convert an arbitrary array into a basic latex
table string (meant to be wrapped inside a {tabular} environment=#

Convertable = Union{Number, AbstractString}

# formats an arbitrary array entry as a string,
# converting digits where possible
function formatString(x::Convertable, n_digits::Int64)::String
    new_x = x
    if (typeof(x) <: Number)
        new_x = round(x, n_digits)
    end
    return string(new_x)
end

function toLatexTable{T<:Any}(a::Array{T,2}; n_digits=2)
    n_rows, n_cols = size(a)

    formatStringSpecialized(x::Convertable) = formatString(x, n_digits)
    @vectorize_1arg Any formatStringSpecialized

    a_formatted = formatStringSpecialized(a)

    table_string = join([join(a_formatted[r,:], " & ") for r=1:n_rows], " \\\\ \n")

    return table_string
end
