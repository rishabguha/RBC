using Optim

include("ModelVariables.jl")

#= These functions =#

# types to specific the model DGP we want to use

abstract ModelType

type ClassicRBC <: ModelType
end

type VariableFrisch <: ModelType
    σ::Float64 # the variable Frisch model has an additional parameter
end


# types to specify the type of data we want
# to simulate
abstract OutputType

type IRF <: OutputType
    shock::Dict{String,Float64}
end

# this lets us use shock as a keyword argument when creating IRF
IRF(;shock=Dict()) = IRF(shock)

type SimulatedData <: OutputType
end



# generates a function which represents the zeros of the
# system of equations that characterizes the RBC model.
# This function is where most model logic lives
function generateModel(constants::ConstantValues, model_type::ModelType)
    α = constants.α
    β = constants.β
    δ = constants.δ
    γ = constants.γ
    ρ = constants.ρ
    labor_share = constants.labor_share

    
    R_star = (1+γ)/β - 1

    capital_share_of_output = α / (((1+γ)/β) - 1 + δ)

    investment_share_of_output = capital_share_of_output * (γ + δ)
    consumption_share_of_output = 1 - investment_share_of_output

    b = 1/(1+((labor_share/(1-labor_share))*(consumption_share_of_output)/(1-α)))
    investment_share_of_capital = investment_share_of_output/capital_share_of_output
    

    
    function f!(λ)
        out_vec = zeros(eltype(λ),15)
        λik, λia, λyk, λya, λck, λca, λRk, λRa, λlk, λla, λkk, λka, λak, λaa = λ

        k = [1,0]
        a = [0,1]

        k_next = [λkk, λka]
        a_next = [λak, λaa]
        
        i = [λik, λia]

        y = [λyk, λya]
        y_next = λyk * k_next + λya * a_next
        c = [λck, λca]
        R = λRk * k_next + λRa * a_next
        l = [λlk, λla]


        # observed leisure is 80%. labor elasticity thing is (l_star)/(1-l_star)
        labor_elasticity_thing = 1 + (0.2/(1.0-0.2))

        # next period consumption
        c_next = λck * k_next + λca * a_next

        # Euler
        out_vec[[1,2]]= R - (c_next - c)

        # interest rate
        out_vec[[3,4]] = ((1+R_star) * R) - (α * (1/capital_share_of_output) * (y_next - k_next))

        # labor-leisure tradeoff depends on model specification
        if (isa(model_type, ClassicRBC))
            out_vec[[5,6]] = (y - c) - (l * (labor_elasticity_thing))
        elseif isa(model_type,  VariableFrisch)
            out_vec[[5,6]] = (y - c) - (l * (model_type.σ + 1))
        end
        

        # production function
        out_vec[[7,8]] = y - (a + (α * k) + ((1-α) * l))

        # investment consumption identity
        out_vec[[9,10]] = ((consumption_share_of_output * c) + (investment_share_of_output) * i) - y

        # capital accumulation 
        out_vec[[11,12]] = (((1+γ) * k_next)  -
                            ((1-δ) * [1,0] + ((investment_share_of_capital) * i)))

        # TFP persistence 
        out_vec[[13,14]] = a_next - (ρ * [0,1])

        # force λkk to be below 1
        out_vec[15] = 1 - max(1, λkk^2)
        return norm(out_vec)
    end
    return f!
end

# generate auxiliary variables, that are simple functions
# of model parameters

function generateAuxiliaryVariables(constants::ConstantValues, main_variables)
    y = main_variables["y"].history
    l = main_variables["l"].history

    w = SimulatedVariable("w", y - l)
    y_l = SimulatedVariable("y_l", y - l)
    return Dict("w"=>w, "y_l"=>y_l)
end


# generate intensive-form data for a given economy

function generateData(constants, T, output_type::OutputType, model_type::ModelType)
    model = generateModel(constants, model_type)

    # if autodiff fails, try without

    solve_model() = try
        optimize(model, ones(14), LBFGS(),
                 Optim.OptimizationOptions(autodiff=true))
    catch
        optimize(model, ones(14), LBFGS())
    end
    
    solution = solve_model()
        

    print(solution)


    # if we want to generate  an IRF we will have a specified shock;
    # otherwise the starting point will be 0.0
    
    if (isa(output_type, IRF))
        shock = output_type.shock
    else
        shock = Dict("k"=>0.0,"a"=>0.0)
    end
    

    λik, λia, λyk, λya, λck, λca, λRk, λRa, λlk, λla, λkk, λka, λak, λaa = solution.minimum

    k = DeterministicVariable("k", makeλ(λkk, λka), [shock["k"]])

    # if the desired data is an IRF then we want to model a as deterministic
    
    if (isa(output_type, IRF))
        a = DeterministicVariable("a", makeλ(λak, λaa), [shock["a"]])
    elseif (isa(output_type,  SimulatedData))
        a = StochasticVariable("a", makeλ(λak, λaa), [shock["a"]], constants.σ)
    end

    y = DeterministicVariable("y", makeλ(λyk, λya))
    l = DeterministicVariable("l", makeλ(λlk, λla))
    c = DeterministicVariable("c", makeλ(λck, λca))
    R = DeterministicVariable("R", makeλ(λRk, λRa))
    i = DeterministicVariable("i", makeλ(λik, λia))

    state_variables =  [k,a]
    response_variables =  [y,l,c,R,i]

    # return simulated state
    main_variables = simulateSystem(state_variables, response_variables, T)

    auxiliary_variables = generateAuxiliaryVariables(constants, main_variables)
    return merge(main_variables, auxiliary_variables)
end


