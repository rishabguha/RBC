using Gadfly

import Base.cor
import Base.std

#= These are convenience functions to aid in the analysis of generated
economic data =#

# plots the history of a given variable
function plot(v::ModelVariable)
    history = v.history
    T = length(history) - 1
    plot(x=0:T, y= history, Geom.line,
         Theme(major_label_font_size=10pt, minor_label_font_size=6pt,
               plot_padding=0.05inch, panel_stroke=colorant"black",
               default_color=colorant"black",
               grid_line_width=0inch),
         Guide.title(v.name), Guide.ylabel(nothing),
         Guide.xlabel(nothing),
         yintercept=[0.0], Geom.hline(color=colorant"red"))
end


function cor(x::ModelVariable, y::ModelVariable)
    return cor(x.history, y.history)
end

function std(x::ModelVariable)
    return std(x.history)
end

#= runs the analysis to plot IRFs and calculate covariance/std deviations
from simulated data. q_num specifies the descriptor string to be attached to the
output files =#

function runAnalysis(constants, model_type::ModelType, q_string; T=100)
    irfs = generateData(constants, 100,
                        IRF(shock=Dict("k"=>0.0, "a"=>0.01)), model_type)

    for (k,v) in irfs
        p = plot(v)
        draw(PDF("$(q_string)$(v.name).pdf", 2.5inch, 2.0inch), p)
    end

    simulated_data = generateSimulatedData(constants, 100, model_type)
    intensive_steady_states = calculateIntensiveSteadyState(constants)

    rows = []
    for name in ["y","c","i","l","a"]
        v = simulated_data[name]

        cor_with_output = cor(v, simulated_data["y"])
        # divide standard deviation by the extensive steady-state mean to
        # get a percentage
        if (name == "l")
            γ = 0
        else
            γ = constants.γ
        end
        
        std_model = 100 * std(v) / (intensive_steady_states[name] *
                                    mean([(1+γ)^t for t=0:T]))
        
        push!(rows, ["\$$name\$", cor_with_output, std_model])
    end        

    table_string = toLatexTable(hcat(["variable", "corr with output", "std (\\%)"],
                                     cat(2,rows...)))

    open("$(q_string)table.tex", "w") do f
        write(f, table_string)
    end
    nothing
end
